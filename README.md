# Sola Scriptura Online

# Présentation

Pour le moment, pour des raisons de droits, n’est disponible que pour la Louis
Segond 1910.

C’est du Python 3, du Flask, du Bootstrap 3 !

## Installation

### Dépendances (Debian 8)

    $ apt-get install python3 texlive-base texlive-base texlive-base texlive-lang-french texlive-fonts-extra

### Backend

    $ cd /var/www
    $ git clone https://framagit.org/kfeine/sola-scriptura.git
    $ chown -R www-data: sola-scriptura
    $ cd sola-scriptura
    $ python3 -m venv venv
    $ source venv/bin/activate
    (venv)$ pip3 install -r requirements.txt

Une fois ces opérations réalisées, on peut faire tourner l’application en
standalone :

    (venv)$ ./run.py

### Reverse proxy : Nginx et Gunicorn sur Debian 8

    $ apt-get install nginx
    $ source /var/www/sola-scriptura/venv/bin/activate
    (venv)$ pip3 install gunicorn

Configuration pour nginx (dans ''/etc/nginx/sites-available'', à lier dans
''/etc/nginx/sites-enabled'') :

    server {
        listen 80;
        listen [::]:80;
        server_name YOURDOMAIN;
        location / {
            include proxy_params;
            proxy_pass http://unix:/var/www/sola-scriptura/sola-scriptura.sock;
            proxy_connect_timeout 300;
            proxy_send_timeout    300;
            proxy_read_timeout    300;
            send_timeout          300;
        }
        access_log /var/log/nginx/sola-scriptura.access.log;
        error_log  /var/log/nginx/sola-scriptura.error.log warn;
    }

Configuration du service gunicorn, dans
''/etc/systemd/system/sola-scriptura.service'' :

    [Unit]
    Description=Gunicorn instance to serve sola scriptura
    After=network.target
    [Service]
    User=www-data
    WorkingDirectory=/var/www/sola-scriptura
    ExecStart=/var/www/sola-scriptura/venv/bin/gunicorn --timeout 240 --log-level warning --error-logfile /var/log/gunicorn/sola-scriptura.log --workers 5 --bind unix:sola-scriptura.sock -m 007 wsgi:app
    [Install]
    WantedBy=multi-user.target

Pour activer tout cela :

    systemctl daemon-reload
    systemctl reload nginx
    systemctl enable sola-scriptura
    systemctl start sola-scriptura

## Licence


## Problèmes connus

- lorsqu’on ne sélectionne pas la version double page, l’en-tête présente sur
  chaque page le titre du livre et du chapitre ; il faudrait raisonner en page
  paire / impaire

## Améliorations à venir

* Gérer des passages
* Page d’explication et de contact
* Faire le ménage dans les regexp
* Améliorer les titres de chapitres pour certaines versions
