from app import app
from flask import render_template, send_file
from .forms import BookForm
from .functions import generate_pdfs
import random, string

output_dir = '/tmp'

@app.route("/", methods=['GET', 'POST'])
def form():
    form = BookForm()
    if form.validate_on_submit():
        pdf_error = False
        # generate random string
        pdf_code = ''.join([random.choice(string.ascii_lowercase + string.digits) for i in range(20)])
        try:
            # generate pdfs
            generate_pdfs(
                output_dir        = output_dir,
                output_name       = pdf_code,
                books             = form.books.data,
                version           = form.version.data,
                title             = form.title.data,
                font_size         = form.font_size.data,
                font              = form.font.data,
                margins           = form.margins.data,
                make_toc          = form.make_toc.data,
                heading_footer    = form.heading_footer.data,
                sections_titles   = form.sections_titles.data,
                booklet_signature = form.signature.data,
                make_booklet      = form.make_booklet.data,
                two_sides         = form.two_sides.data,
            )
        except:
            pdf_error = True
        # return page with links
        return render_template("form.html.j2",
                               form=form,
                               form_success=True,
                               booklet=form.make_booklet.data,
                               pdf_error=pdf_error,
                               pdf_code=pdf_code,
                               title='Home')
    return render_template("form.html.j2",
                           form=form,
                           title='Home')

@app.route("/getpdf/<code>.pdf", methods=['GET', 'POST'])
def get_pdf(code, booklet=False):
    return send_file("%s/%s.pdf" % (output_dir, code))

@app.route("/a-propos", methods=['GET'])
def about():
    return render_template("about.html.j2", about_page=True)

@app.route("/droits", methods=['GET'])
def rights():
    return render_template("rights.html.j2", rights_page=True)

if __name__ == "__main__":
    app.run()
