import re
import os.path
import subprocess
import shutil
from jinja2 import Environment, FileSystemLoader
from .variables import livresBible

PATH = os.path.dirname(os.path.abspath(__file__))
# see http://eosrei.net/articles/2015/11/latex-templates-python-and-jinja2-generate-pdfs
LATEX_JINJA_ENV = Environment(
    block_start_string    = '\BLOCK{',
    block_end_string      = '}',
    comment_start_string  = '\#{',
    comment_end_string    = '}',
    variable_start_string = '\VAR{',
    variable_end_string   = '}',
    line_statement_prefix = '%%',
    trim_blocks           = True,
    autoescape            = False,
    loader                = FileSystemLoader(os.path.join(PATH, 'templates'))
)
FNULL = open(os.devnull, 'w')
PDFLATEX_BIN = shutil.which("pdflatex")

def parse_html(book_content, make_parts, sections_titles, book_title, version):
    # We must be careful, replacemants must be in the right order, because
    # a character mean somtime different depending on the context.
    # For example, the HTML ASCCII code for ’ will be replace by ’ or by \fg{}

    if version == "lsg":
        # 7 spans
        # spans that may be nested in others must be removed first
        book_content = re.sub("<span class=\"content\">(.*?)</span>", r"\1", book_content)
        # then we can process other spans
        book_content = re.sub("<span class=\"label\">.*?</span>", "", book_content)
        book_content = re.sub("<span class=\"it\">([^<]*)</span>", r"\\textit{\1}", book_content)
        book_content = re.sub("<span class=\"wj\">([^<]*)</span>", r"\1", book_content)
        book_content = re.sub("<span class=\"heading\">(.*?)</span>", r"\1", book_content)
        # almost always for "pause" mention in Psalms
        book_content = re.sub("<span class=\"qs\">(.*?)</span>", r"\\hfill\\begin{itshape}\1\\end{itshape}\\\\ \\vspace{4mm}", book_content)
        # then we can process other spans
        book_content = re.sub("<span class=\"verse v\d+\" [^>]*>(.*?)</span>", r"\1", book_content)

        # 11 divs
        # titles
        if sections_titles:
            # remove chapter indications
            book_content = re.sub("<div class=\"s\">(.*?)</div>", r"\n{\\let\\cleardoublepage\\relax\\chapter{\1}}\n\n", book_content)
        else:
            book_content = re.sub("<div class=\"s\">.*?</div>", "", book_content)
        # delete things we don't need
        book_content = re.sub("<div class=\"label\">.*?</div>", "", book_content)
        book_content = re.sub("<div class=\"mr\">.*?</div>", "", book_content)
        book_content = re.sub("<div class=\"r\">.*?</div>", "", book_content)
        book_content = re.sub("<div class=\"ms1\">.*?</div>", "", book_content)
        book_content = re.sub("<div class=\"ms2\">.*?</div>", "", book_content)
        # breaks
        book_content = re.sub("<div class=\"b\"></div>", r"\\vspace{4mm}", book_content)
        # paragraphs
        book_content = re.sub("<div class=\"p\">(.*?)</div>", r"\1 \n\n", book_content)
        book_content = re.sub("<div class=\"m\">(.*?)</div>", r"\1 \n\n", book_content)
        # quotes
        book_content = re.sub("<div class=\"q\">(.*)</div>", r"\n\\begin{verse}\1\\end{verse}\n", book_content)
        # consecutives q is for newlines
        book_content = re.sub("</div><div class=\"q\">", r"\\\\ ", book_content)
        book_content = re.sub(r"</div>\\vspace{4mm}<div class=\"q\">", r"\\\\ \\vspace{4mm}", book_content)
        book_content = re.sub("<div class=\"pi\">(.*)</div>", r"\n\\begin{verse}\1\\end{verse}\n", book_content)

        # some errors
        book_content = re.sub(r"\\vspace{4mm}\\\\", "", book_content)

    elif version == "s21":
        # 9 spans
        # spans that may be nested in others must be removed first
        book_content = re.sub("<span class=\"fr\">[^<]*</span>", "", book_content)
        book_content = re.sub("<span class=\"ft\">[^<]*</span>", "", book_content)
        book_content = re.sub("<span class=\"content\">([^<]*)</span>", r"\1", book_content)
        book_content = re.sub("<span class=\"label\">[^<]*</span>", "", book_content)
        # then we can process other spans
        book_content = re.sub("<span class=\" body\">[^<]*</span>", "", book_content)
        book_content = re.sub("<span class=\"it\">([^<]*)</span>", r"\\textit{\1}", book_content)
        book_content = re.sub("<span class=\"bd\">([^<]*)</span>", r"\\textit{\1}", book_content)
        book_content = re.sub("<span class=\"note f\">[^<]*</span>", "", book_content)
        # then we can process other spans
        book_content = re.sub("<span class=\"verse v\d+\" [^>]*>([^<]*)</span>", r"\1", book_content)
        # other spans, for them order don't matter
        book_content = re.sub("<span class=\"heading\">([^<]*)</span>", r"\1", book_content)

        # 8 divs
        # delete things we don't need
        book_content = re.sub("<div class=\"r\">([^<]*)</div>", "", book_content)
        book_content = re.sub("<div class=\"label\">([^<]*)</div>", "", book_content)
        book_content = re.sub("<div class=\"s2\">([^<]*)</div>", "", book_content)
        book_content = re.sub("<div class=\"s3\">([^<]*)</div>", "", book_content)
        # paragraphs
        book_content = re.sub("<div class=\"p\">(.*?)</div>", r"\1 \n\n", book_content)

        # do we keep titles?
        if sections_titles:
            # remove chapter indications
            book_content = re.sub("<div class=\"s1\">([^<]*?) \d.*</div>", r"\n{\\let\\cleardoublepage\\relax\\chapter{\1}}\n\n", book_content)
        else:
            book_content = re.sub("<div class=\"s1\">([^<]*)</div>", "", book_content)

    elif version == "nbs":
        # 15 types of span
        # spans that may be nested in others must be removed first
        book_content = re.sub("<span class=\"heading\">([^<]*)</span>", r"\1", book_content)
        book_content = re.sub("<span class=\"content\">([^<]*)</span>", r"\1", book_content)
        book_content = re.sub("<span class=\"ord\">[^<]*</span>", "", book_content)
        book_content = re.sub("<span class=\"fr\">[^<]*</span>", "", book_content)
        book_content = re.sub("<span class=\"label\">[^<]*</span>", "", book_content)
        book_content = re.sub("<span class=\"fk\">[^<]*</span>", "", book_content)
        # then we can process other spans
        book_content = re.sub("<span class=\"sc\">([^<]*)</span>", r"\\textsc{\1}", book_content)
        book_content = re.sub("<span class=\"it\">([^<]*)</span>", r"\\textit{\1}", book_content)
        book_content = re.sub("<span class=\"fq\">[^<]*</span>", "", book_content)
        book_content = re.sub("<span class=\"qs\">[^<]*</span>", "", book_content)
        # then we can process other spans
        book_content = re.sub("<span class=\"fqa\">[^<]*</span>", "", book_content)
        book_content = re.sub("<span class=\"ft\">[^<]*</span>", "", book_content)
        # then we can process other spans
        book_content = re.sub("<span class=\" body\">[^<]*</span>", "", book_content)
        # then we can process other spans
        book_content = re.sub("<span class=\"note f\">[^<]*</span>", "", book_content)
        # other spans, for them order don't matter
        book_content = re.sub("<span class=\"verse v\d+\" [^>]*>([^<]*)</span>", r"\1", book_content)

        # there are only divs left
        # there are 13 classes of divs
        # do we keep titles?
        if sections_titles:
            book_content = re.sub("<div class=\"s1\">([^<]*)</div>", r"\n{\\let\\cleardoublepage\\relax\\chapter{\1}}\n\n", book_content)
        else:
            book_content = re.sub("<div class=\"s1\">([^<]*)</div>", "", book_content)
        # delete things we don't need
        book_content = re.sub("<div class=\"r\">([^<]*)</div>", "", book_content)
        book_content = re.sub("<div class=\"label\">([^<]*)</div>", "", book_content)
        book_content = re.sub("<div class=\"sp\">([^<]*)</div>", "", book_content)
        # introduce spaces where titles were
        book_content = re.sub("<div class=\"ms1\">([^<]*)</div>", r"\\vspace{4mm}", book_content)
        book_content = re.sub("<div class=\"s1\">([^<]*)</div>", r"\\vspace{4mm}", book_content)
        book_content = re.sub("<div class=\"s2\">([^<]*)</div>", r"\\vspace{4mm}", book_content)
        # for psalms
        book_content = re.sub("<div class=\"d\">(.*?)</div>", r"\n\\vspace{4mm}\n\\textit{\1} \n\n", book_content)
        # paragraphs
        book_content = re.sub("<div class=\"p\">(.*?)</div>", r"\1 \n\n", book_content)
        book_content = re.sub("<div class=\"m\">(.*?)</div>", r"\n\1 \n\n", book_content)
        # quotes
        book_content = re.sub("<div class=\"b\">([^<]*)</div>", r"\\\\ \\vspace{4mm}", book_content)
        book_content = re.sub("<div class=\"q1\">(.*)</div>", r"\n\\begin{verse}\1\\end{verse}\n", book_content)
        book_content = re.sub("<div class=\"q2\">(.*)</div>", r"\n\\begin{verse}\1\\end{verse}\n", book_content)
        # consecutives q1/2/3 is for newlines
        book_content = re.sub(r"</div>\\vspace{4mm}<div class=\"q1\">", r"\\\\ \\vspace{4mm}", book_content)
        book_content = re.sub(r"</div><div class=\"q1\">", r"\\\\", book_content)
        book_content = re.sub(r"</div>\\vspace{4mm}<div class=\"q2\">", r"\\\\ \\vspace{4mm}", book_content)
        book_content = re.sub(r"</div><div class=\"q2\">", r"\\\\", book_content)
        book_content = re.sub(r"</div>\\vspace{4mm}<div class=\"q3\">", r"\\\\ \\vspace{4mm}", book_content)
        book_content = re.sub(r"</div><div class=\"q3\">", r"\\\\", book_content)

        # remaining stuff
        book_content = re.sub("</div>", "", book_content)
        book_content = re.sub("<div [^>]*>", "", book_content)

    elif version == "bds":
        # 17 types of span
        # spans that may be nested in others must be removed first
        book_content = re.sub("<span class=\"heading\">([^<]*)</span>", r"\1", book_content)
        book_content = re.sub("<span class=\"content\">([^<]*)</span>", r"\1", book_content)
        book_content = re.sub("<span class=\"label\">[^<]*</span>", "", book_content)
        book_content = re.sub("<span class=\"bk\">[^<]*</span>", "", book_content)
        book_content = re.sub("<span class=\"sc\">([^<]*)</span>", r"\\textsc{\1}", book_content)
        # then we can process other spans
        book_content = re.sub("<span class=\"ord\">([^<]*)</span>", r"\up{\1}", book_content)
        book_content = re.sub("<span class=\"tl\">([^<]*)</span>", r"\up{\1}", book_content)
        book_content = re.sub("<span class=\"qt\">([^<]*)</span>", r"\\textit{\1}", book_content)
        book_content = re.sub("<span class=\"fr\">[^<]*</span>", "", book_content)
        book_content = re.sub("<span class=\"em\">([^<]*)</span>", r"\textit{\1}", book_content)
        # then we can process other spans
        book_content = re.sub("<span class=\"ft\">[^<]*</span>", "", book_content)
        book_content = re.sub("<span class=\"fqa\">[^<]*</span>", "", book_content)
        book_content = re.sub("<span class=\"fq\">[^<]*</span>", "", book_content)
        book_content = re.sub("<span class=\"fv\">[^<]*</span>", "", book_content)
        # then we can process other spans
        book_content = re.sub("<span class=\" body\">[^<]*</span>", "", book_content)
        # then we can process other spans
        book_content = re.sub("<span class=\"note f\">[^<]*</span>", "", book_content)
        # then we can process other spans
        book_content = re.sub("<span class=\"verse [^\"]*\" [^>]*>([^<]*)</span>", r"\1", book_content)

        # 15 types of divs
        # breaks
        book_content = re.sub("<div class=\"b\"></div>", r"\\vspace{4mm}", book_content)
        # useless divs
        book_content = re.sub("<div class=\"label\">[^<]*</div>", "", book_content)
        book_content = re.sub("<div class=\"r\">[^<]*</div>", "", book_content)
        # introduce spaces where titles were
        book_content = re.sub("<div class=\"s1\">([^<]*)</div>", r"\\vspace{4mm}", book_content)
        book_content = re.sub("<div class=\"s2\">([^<]*)</div>", r"\\vspace{4mm}", book_content)
        # Psalms intros
        book_content = re.sub("<div class=\"d\">([^<]*)</div>", r"\\textit{\1} \\vspace{4mm}", book_content)
        # create paragraphs
        # the first one is for an exception, when a div is splited across 2 chapters
        # (it only happens twice in all BDS)
        book_content = re.sub("</div>\s*<div class=\"nb\">", "", book_content, flags=re.MULTILINE)
        book_content = re.sub("<div class=\"p\">(.*?)</div>", r"\1 \n\n", book_content)
        book_content = re.sub("<div class=\"m\">(.*?)</div>", r"\1 \n\n", book_content)
        # titles
        if sections_titles:
            book_content = re.sub("<div class=\"ms1\">(.*?)</div>", r"\n{\\let\\cleardoublepage\\relax\\chapter{\1}}\n\n", book_content)
        else:
            book_content = re.sub("<div class=\"ms1\">[^<]*</div>", "", book_content)
        # manage quotes
        book_content = re.sub("<div class=\"pm\">(.*)</div>", r"\n\\begin{quote}\\begin{itshape}\1\\end{itshape}\\end{quote}\n", book_content)
        book_content = re.sub("</div><div class=\"pm\">", r"\\\\", book_content)
        # kind of lists
        book_content = re.sub("<div class=\"li2\">([^<]*)</div>", r"\1", book_content)
        book_content = re.sub("<div class=\"li1\">([^<]*)</div>", r"\1 \\\\", book_content)
        # almost always for "pause" mention in Psalms
        book_content = re.sub("<div class=\"q4\">([^<]*)</div>", r"\\hfill\\begin{itshape}\1\\end{itshape}\n\n", book_content)
        # let's play with quotes / verses
        book_content = re.sub(r"\\vspace{4mm}\\vspace{4mm}", "", book_content)
        book_content = re.sub("<div class=\"q1\">(.*)</div>", r"\n\\begin{verse}\1\\end{verse}\n", book_content)
        # consecutives q1 is for newlines
        book_content = re.sub("</div><div class=\"q1\">", r"\\\\ ", book_content)
        book_content = re.sub(r"</div>\\vspace{4mm}<div class=\"q1\">", r"\\\\ \\vspace{4mm}", book_content)
        # # latex doesn't like \\ [
        book_content = re.sub(r"\\ \[", r"\\ {[}", book_content)
        # book_content = re.sub("<div class=\"(ms1|s1|s2|r|b)\">(\\vspace{4mm})?</div>", r"\\vspace{4mm}", book_content)
        # book_content = re.sub("<div class=\"li1\">([^<]*)</div>", r"\1 \\\\", book_content)

    # Common with all version

    # ASCII. Whyyyyy?
    # accents and other characters
    book_content = re.sub("&#146;", "’", book_content) # let latex manage nbsp
    book_content = re.sub("&#160;", " ", book_content) # let latex manage nbsp
    book_content = re.sub("&#176;", "°", book_content)
    book_content = re.sub("&#192;", "À", book_content)
    book_content = re.sub("&#199;", "Ç", book_content)
    book_content = re.sub("&#200;", "È", book_content)
    book_content = re.sub("&#201;", "É", book_content)
    book_content = re.sub("&#202;", "Ê", book_content)
    book_content = re.sub("&#203;", "Ë", book_content)
    book_content = re.sub("&#206;", "Î", book_content)
    book_content = re.sub("&#207;", "Ï", book_content)
    book_content = re.sub("&#212;", "Ô", book_content)
    book_content = re.sub("&#217;", "Ù", book_content)
    book_content = re.sub("&#220;", "Ü", book_content)
    book_content = re.sub("&#224;", "à", book_content)
    book_content = re.sub("&#226;", "â", book_content)
    book_content = re.sub("&#231;", "ç", book_content)
    book_content = re.sub("&#232;", "è", book_content)
    book_content = re.sub("&#233;", "é", book_content)
    book_content = re.sub("&#234;", "ê", book_content)
    book_content = re.sub("&#235;", "ë", book_content)
    book_content = re.sub("&#238;", "î", book_content)
    book_content = re.sub("&#239;", "ï", book_content)
    book_content = re.sub("&#244;", "ô", book_content)
    book_content = re.sub("&#249;", "ù", book_content)
    book_content = re.sub("&#251;", "û", book_content)
    book_content = re.sub("&#252;", "ü", book_content)
    book_content = re.sub("&#338;", "Œ", book_content)
    book_content = re.sub("&#339;", "œ", book_content)
    book_content = re.sub("&#8211;", "—", book_content)
    book_content = re.sub("&#8212;", "—", book_content)
    book_content = re.sub("&#8230;", "…", book_content)
    # quotes - order matters
    # book_content = re.sub("&#171;", r"\\og ", book_content)
    book_content = re.sub("&#171;", r"«~", book_content)
    # book_content = re.sub("&#187;", r" \\fg{}", book_content)
    book_content = re.sub("&#187;", r"~»", book_content)
    book_content = re.sub("&#8216;", " `` ", book_content)
    book_content = re.sub("&#8217; ", " '' ", book_content)
    #book_content = re.sub("&#8216;", "‘", book_content)
    book_content = re.sub("&#8217;", "’", book_content)
    book_content = re.sub("&#8220;", "`` ", book_content)
    book_content = re.sub("&#8221;", " ''", book_content)
    book_content = re.sub("&#8239;", r"\\ ", book_content)
    book_content = re.sub("&#8198;", r"\\ ", book_content)
    book_content = re.sub("&#160;", r"\\ ", book_content)

    # Titles as parts?
    if make_parts:
        book_content = "\part{%s}\n\n" % book_title + book_content

    return book_content


def generate_pdfs(
        output_dir,
        output_name,
        books,
        version,
        title,
        font_size,
        font,
        margins,
        make_toc,
        heading_footer,
        sections_titles,
        booklet_signature,
        make_booklet,
        two_sides,
    ):

    books_titles   = []
    books_contents = []
    verbose        = False

    if len(books) == 1:
        make_parts = False
    else:
        make_parts = True

    for book in books:
        # load html file
        book_html = "bibles/%s/%s" % (version, book)

        with open(book_html, "r") as book_html_f:
            book_title = dict(livresBible)[book]
            books_titles = books_titles + [ book_title ]
            book_content = book_html_f.read()

        parsed_content = parse_html(
            book_content,
            make_parts,
            sections_titles,
            book_title,
            version
        )
        books_contents = books_contents + [ parsed_content ]

    template_book = LATEX_JINJA_ENV.get_template('book.tex.j2')
    books_tex = template_book.render(
        title          = title or ", ".join(books_titles),
        books_contents = books_contents,
        make_toc       = make_toc,
        font           = font,
        font_size      = font_size,
        margins        = margins,
        heading_footer = heading_footer,
        two_sides      = two_sides,
        titles_serif   = True,
    )
    with open("%s/%s.tex" % (output_dir, output_name), "w") as books_tex_f:
        # write to file
        books_tex_f.write(books_tex)

    if make_toc:
        subprocess.call([PDFLATEX_BIN, "-draftmode", "-output-directory", output_dir, "%s/%s.tex" % (output_dir, output_name)], stdout=FNULL)
    subprocess.call([PDFLATEX_BIN, "-output-directory", output_dir, "%s/%s.tex" % (output_dir, output_name)], stdout=FNULL)

    # make booklet
    if make_booklet:
        template_booklet = LATEX_JINJA_ENV.get_template('booklet.tex.j2')
        booklet_tex = template_booklet.render(
            output_dir        = output_dir,
            output_name       = output_name,
            booklet_signature = booklet_signature
        )
        with open("%s/%s-booklet.tex" % (output_dir, output_name), "w") as booklet_f:
            # write to file
            booklet_f.write(booklet_tex)
        subprocess.call([PDFLATEX_BIN, "-output-directory", output_dir, "%s/%s-booklet.tex" % (output_dir, output_name)], stdout=FNULL)
