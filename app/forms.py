from flask_wtf import FlaskForm
from wtforms.widgets.html5 import NumberInput
from wtforms import TextField, BooleanField, IntegerField, validators
from wtforms_components import SelectField, SelectMultipleField
from .variables import livresBible, margins, signatures, fonts, versionsBible


class BookForm(FlaskForm):
    version         = SelectField("Version", [validators.required("Veuillez sélectionner la traduction")], choices=versionsBible)
    books           = SelectMultipleField("Livres", [validators.required("Veuillez choisir au moins un livre")], choices=livresBible)
    title           = TextField("Titre", [validators.optional()])
    font            = SelectField("Police", [validators.optional()], choices=fonts)
    font_size       = IntegerField("Taille de la police", [validators.optional()], widget=NumberInput(), default=12)
    margins         = SelectField("Taille de la zone de texte", [validators.optional()], choices=margins, default='10')
    sections_titles = BooleanField("Inclure les titres des parties (non inspiré)", [validators.optional()])
    make_toc        = BooleanField("Inclure la table des matières (plus long à générer)", [validators.optional()])
    heading_footer  = BooleanField("Faire une en-tête (titre / chapitre) et pied-de-page (pages)", [validators.optional()], default=True)
    signature       = SelectField("Signature", [validators.optional()], choices=signatures)
    make_booklet    = BooleanField("Faire le PDF pour la reluire (plus long à générer)", [validators.optional()])
    two_sides       = BooleanField("Mode page double (conseillé pour la reliure)", [validators.optional()])
